import { Component, OnInit } from '@angular/core';
import { FavoritesService } from '../../services/favorites.service';
import { CurrentConditions } from '../../models/current-conditions';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {

  favorites: CurrentConditions;

  constructor(private favoritesService: FavoritesService) { }

  ngOnInit(): void {
    this.updateFavorites();
  }

  updateFavorites() {
    this.favorites = this.favoritesService.getFavorite();
  }



}
// filteredCities: Observable<string[]>;
  
  
// constructor(private weatherService: WeatherService) { }

// ngOnInit() {
//   this.filteredCities = this.control.valueChanges.pipe(
//    startWith(''),
//    map(value => this.filter(value))
//   );
// }

// private filter(value: string): string[]{
//   this.search = value;
//   console.log(value);
//   this.weatherService.getLocations(value).subscribe((data: any) => {
//     if(!data) return [];
//     this.cities = data.map(item => item.LocalizedName || '');
//     this.cityID = data[0]
//     return data;
//   });
//   return this.cities;