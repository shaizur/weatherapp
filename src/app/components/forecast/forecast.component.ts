import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { WeatherService } from '../../services/weather.service';
import { Subscription } from 'rxjs';
import { CurrentConditions } from '../../models/current-conditions';
import { DailyForecasts, FiveDaysForecast } from '../../models/five-days-forcast';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit, OnDestroy {
  
  subscription: Subscription;
  defaultCity: CurrentConditions;
  fiveDaysForcast: FiveDaysForecast;
  cityID: Number = 215854;
  dailyForecasts: DailyForecasts[] = [];
  @Input () currentCity: any;
  @Input () currentForcast: any;

  constructor(private weatherService: WeatherService) { }

  ngOnInit(): void {
    this.subscription = this.weatherService.getTelAviv().subscribe((data: CurrentConditions) => {
      if(!data[0]) return {};
      this.defaultCity = data[0];
    });

    this.weatherService.getFiveDaysForecast(this.cityID).subscribe((data: FiveDaysForecast) => {
      this.dailyForecasts = data.DailyForecasts;
      //console.log(this.fiveDaysForcast);
    });
  }
  
  ngOnDestroy() {
  }

}
